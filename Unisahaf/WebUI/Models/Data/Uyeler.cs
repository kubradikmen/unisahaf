﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.Models.Data
{
    public class Uyeler
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string eMail { get; set; }

        [Required]
        [StringLength(20)]
        public string sifre { get; set; }

        [Required]
        public int uyeTip { get; set; }
    }
}