﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ünisahaf.Startup))]
namespace ünisahaf
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
