﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ünisahaf.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "HAKKIMIZDA";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İLETİŞİM";

            return View();
        }
        public ActionResult Kitap()
        {
            return View();
        }
        public ActionResult DersNotu()
        {
            return View();
        }
        public ActionResult VizeFinal()
        {
            return View();
        }
    }
}